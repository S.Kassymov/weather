package com.example.weather

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.weather.models.FullWeather.FullWeatherResponse
import com.example.weather.utils.Constants
import com.google.android.gms.location.*
import com.google.gson.Gson
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.squareup.okhttp.Callback
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import com.squareup.okhttp.Response
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import retrofit.Call
import retrofit.GsonConverterFactory
import retrofit.Retrofit
import java.io.IOException
import java.lang.Math.round
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var mWeather: FullWeatherResponse
    private  lateinit var array:DoubleArray


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)




        if(!isLocationEnabled()){
            Toast.makeText(
                    this,
                    "Your location provider is turned off. Please turn it on.",
                    Toast.LENGTH_SHORT
            ).show()

            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }else{
            Toast.makeText(
                    this,
                    "Your location provider is ON",
                    Toast.LENGTH_SHORT
            ).show()

        }


        cityButton.setOnClickListener(){
            val intent = Intent(this, CityActivity::class.java)
            startActivity(intent)
        }


        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                )
                .withListener(object : MultiplePermissionsListener{
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?){
                        if(report!!.areAllPermissionsGranted()){
                            requestLocationData()
                        }
                        if(report.isAnyPermissionPermanentlyDenied){
                            Toast.makeText(
                                    this@MainActivity,
                                    "You have denied location permission. Please allow it is mandatory.",
                                    Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        showRationalDialogForPermissions()
                    }
                }).onSameThread()
                .check()

        array = intent.getSerializableExtra("array") as DoubleArray
        getApi("https://api.openweathermap.org/data/2.5/onecall?lat=${array[0]}&lon=${array[1]}&exclude=minutely&appid=093be2e4bbe5afcb6fdddc173a047f96")

        val timer = object: CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}

            override fun onFinish() {
               updateWeather()
                loadingPanel.isVisible = false
                layoutHour.setBackgroundColor(Color.parseColor("#1E1E45"))
                layoutWeek.setBackgroundColor(Color.parseColor("#1E1E45"))

            }
        }
        timer.start()

    }

    private fun showRationalDialogForPermissions(){
        AlertDialog.Builder(this)
            .setMessage("It looks like you have turned off permissions required for this feature. It can be enabled under Application Settings.")
                .setPositiveButton(
                        "GO TO SETTINGS"
                ){_,_->
                    try{
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        var uri = Uri.fromParts("package",packageName, null)
                        intent.data = uri
                        startActivity(intent)
                    }catch (e: ActivityNotFoundException){
                        e.printStackTrace()
                    }
                }
                .setNegativeButton("Cancel"){dialog,
                                       _->
                    dialog.dismiss()
                }
        }

    @SuppressWarnings("MissingPermission")
    private fun requestLocationData(){
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        )
    }

    private val mLocationCallback = object: LocationCallback(){
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            val latitude = mLastLocation.latitude
            Log.i("Current latitude", "$latitude")

            val longitude = mLastLocation.longitude
            Log.i("Current longitude","$longitude")

            getLocationWeatherDetails()
        }
    }

    private fun isLocationEnabled(): Boolean{
        var locationManager: LocationManager =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return  locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER)
    }

    private fun getLocationWeatherDetails(){
        if(Constants.isNetworkAvailable(this@MainActivity)){

            Toast.makeText(
                this@MainActivity,
                "You have connected to the internet. Now you can make an api call",
                Toast.LENGTH_SHORT
            ).show()
        }else{
            Toast.makeText(
                this@MainActivity,
                "No internet connection available",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun getApi(url :String) {

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback {

            override fun onResponse(response: Response?) {

                val body = response?.body()?.string()
                if(body!=null) {
                        mWeather = Gson().fromJson(body, FullWeatherResponse::class.java)
                }
            }
            override fun onFailure(request: Request?, e: IOException?) {
                Log.d("Response:","error")
            }
        })
    }





    private fun updateWeather(){
        val c = Calendar.getInstance()
        val weekdays = arrayOf<String>("Mon","Sun","Tue","Wed","Thu","Fri","Sat")
        val dayWeek: Int = c.get(Calendar.DAY_OF_WEEK)

        val hour = c.get(Calendar.HOUR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        idDate.text = "${day}.${month+1}.2021"

        val iconUrl = "http://openweathermap.org/img/w/" +  mWeather.current.weather[0].icon + ".png"
        Picasso.with(this).load(iconUrl).into(icon)

        when(true){
            mWeather.lat ==51.10 && mWeather.lon==71.26 -> cityButton.text = "Nur-Sultan"
            mWeather.lat==43.22 && mWeather.lon==76.85 -> cityButton.text = "Almaty"
            mWeather.lat==40.71 && mWeather.lon==-74.00 -> cityButton.text = "New-York"
            mWeather.lat==55.75 && mWeather.lon==37.61 -> cityButton.text = "Moscow"
            mWeather.lat==51.30 && mWeather.lon==0.08 -> cityButton.text = "London"
            mWeather.lat==35.41 && mWeather.lon==139.42 -> cityButton.text = "Tokyo"
            mWeather.lat==48.51 && mWeather.lon==48.51 -> cityButton.text = "Paris"
            mWeather.lat==30.03 && mWeather.lon==31.14 -> cityButton.text = "Cairo"
            mWeather.lat==-35.18 && mWeather.lon==149.07-> cityButton.text = "Canberra"
            mWeather.lat==-15.48 && mWeather.lon==-47.52-> cityButton.text = "Brazilia"
        }
        weatherStatus.text = mWeather.current.weather[0].description
        windStatus.text = "${mWeather.current.wind_speed} m/s"
        tempStatus.text = "${round(mWeather.current.temp -273.15)} C°"
        humidityStatus.text = "humidity: ${mWeather.current.humidity}%"
        feelslikeStatus.text = "feels like: ${round(mWeather.current.feels_like-273.15)} C°"
        pressureStatus.text = "${0.7500*mWeather.current.pressure}"



        //daily
        //1
        weekday1.text = weekdays[dayWeek%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.daily[1].weather[0].icon + ".png").into(iconWeek1)
        weekdayTemp1.text = "${round(mWeather.daily[1].temp.day - 273.15)} C°"
        weeknightTemp1.text = "${round(mWeather.daily[1].temp.night - 273.15)} C°"
        //1
        weekday2.text = weekdays[(dayWeek+1)%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +   mWeather.daily[2].weather[0].icon + ".png").into(iconWeek2)
        weekdayTemp2.text = "${round(mWeather.daily[2].temp.day - 273.15)} C°"
        weeknightTemp2.text = "${round(mWeather.daily[2].temp.night - 273.15)} C°"
        //1
        weekday3.text = weekdays[(dayWeek+2)%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +   mWeather.daily[3].weather[0].icon + ".png").into(iconWeek3)
        weekdayTemp3.text ="${round(mWeather.daily[3].temp.day - 273.15)} C°"
        weeknightTemp3.text = "${round(mWeather.daily[3].temp.night - 273.15)} C°"
        //
        weekday4.text = weekdays[(dayWeek+3)%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.daily[4].weather[0].icon + ".png").into(iconWeek4)
        weekdayTemp4.text ="${round(mWeather.daily[4].temp.day - 273.15)} C°"
        weeknightTemp4.text = "${round(mWeather.daily[4].temp.night - 273.15)} C°"
        //1
        weekday5.text = weekdays[(dayWeek+4)%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +   mWeather.daily[5].weather[0].icon + ".png").into(iconWeek5)
        weekdayTemp5.text = "${round(mWeather.daily[5].temp.day - 273.15)} C°"
        weeknightTemp5.text ="${round(mWeather.daily[5].temp.night - 273.15)} C°"
        //1
        weekday6.text = weekdays[(dayWeek+5)%6]
        Picasso.with(this).load("http://openweathermap.org/img/w/" +   mWeather.daily[6].weather[0].icon + ".png").into(iconWeek6)
        weekdayTemp6.text = "${round(mWeather.daily[6].temp.day - 273.15)} C°"
        weeknightTemp6.text = "${round(mWeather.daily[6].temp.night - 273.15)} C°"

        //hourly
        //1
        timeHour1.text = "${(hour+1)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[1].weather[0].icon + ".png").into(iconHour1)
        hourTemp1.text = "${round(mWeather.hourly[1].temp - 273.15)} C°"
        //1
        timeHour2.text = "${(hour+2)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[2].weather[0].icon + ".png").into(iconHour2)
        hourTemp2.text = "${round(mWeather.hourly[2].temp - 273.15)} C°"
        //1
        timeHour3.text = "${(hour+3)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[3].weather[0].icon + ".png").into(iconHour3)
        hourTemp3.text = "${round(mWeather.hourly[3].temp - 273.15)} C°"
        //1
        timeHour4.text = "${(hour+4)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[4].weather[0].icon + ".png").into(iconHour4)
        hourTemp4.text = "${round(mWeather.hourly[4].temp - 273.15)} C°"
        //1
        timeHour5.text = "${(hour+5)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[5].weather[0].icon + ".png").into(iconHour5)
        hourTemp5.text = "${round(mWeather.hourly[5].temp - 273.15)} C°"
        //1
        timeHour6.text = "${(hour+6)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[6].weather[0].icon + ".png").into(iconHour6)
        hourTemp6.text = "${round(mWeather.hourly[6].temp - 273.15)} C°"
        //1
        timeHour7.text = "${(hour+7)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[7].weather[0].icon + ".png").into(iconHour7)
        hourTemp7.text = "${round(mWeather.hourly[7].temp - 273.15)} C°"
        //1
        timeHour8.text = "${(hour+8)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[8].weather[0].icon + ".png").into(iconHour8)
        hourTemp8.text = "${round(mWeather.hourly[8].temp - 273.15)} C°"
        //1
        timeHour9.text = "${(hour+9)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[9].weather[0].icon + ".png").into(iconHour9)
        hourTemp9.text = "${round(mWeather.hourly[9].temp - 273.15)} C°"
        //1
        timeHour10.text = "${(hour+10)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[10].weather[0].icon + ".png").into(iconHour10)
        hourTemp10.text = "${round(mWeather.hourly[10].temp - 273.15)} C°"
        //1
        timeHour11.text = "${(hour+11)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[11].weather[0].icon + ".png").into(iconHour11)
        hourTemp11.text = "${round(mWeather.hourly[11].temp - 273.15)} C°"
        //1
        timeHour12.text = "${(hour+12)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[12].weather[0].icon + ".png").into(iconHour12)
        hourTemp12.text = "${round(mWeather.hourly[12].temp - 273.15)} C°"
        //1
        timeHour13.text = "${(hour+13)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[13].weather[0].icon + ".png").into(iconHour13)
        hourTemp13.text = "${round(mWeather.hourly[13].temp - 273.15)} C°"
        //1
        timeHour14.text = "${(hour+14)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[14].weather[0].icon + ".png").into(iconHour14)
        hourTemp14.text = "${round(mWeather.hourly[14].temp - 273.15)} C°"
        //1
        timeHour15.text = "${(hour+15)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[15].weather[0].icon + ".png").into(iconHour15)
        hourTemp15.text = "${round(mWeather.hourly[15].temp - 273.15)} C°"
        //1
        timeHour16.text = "${(hour+16)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[16].weather[0].icon + ".png").into(iconHour16)
        hourTemp16.text = "${round(mWeather.hourly[16].temp - 273.15)} C°"
        //1
        timeHour17.text = "${(hour+17)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[17].weather[0].icon + ".png").into(iconHour17)
        hourTemp17.text = "${round(mWeather.hourly[17].temp - 273.15)} C°"
        //1
        timeHour18.text = "${(hour+18)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[18].weather[0].icon + ".png").into(iconHour18)
        hourTemp18.text = "${round(mWeather.hourly[18].temp - 273.15)} C°"
        //1
        timeHour19.text = "${(hour+19)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[19].weather[0].icon + ".png").into(iconHour19)
        hourTemp19.text = "${round(mWeather.hourly[19].temp - 273.15)} C°"
        //1
        timeHour20.text = "${(hour+20)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[20].weather[0].icon + ".png").into(iconHour20)
        hourTemp20.text = "${round(mWeather.hourly[20].temp - 273.15)} C°"
        //1
        timeHour21.text = "${(hour+21)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[21].weather[0].icon + ".png").into(iconHour21)
        hourTemp21.text = "${round(mWeather.hourly[21].temp - 273.15)} C°"
        //1
        timeHour22.text = "${(hour+22)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[22].weather[0].icon + ".png").into(iconHour22)
        hourTemp22.text = "${round(mWeather.hourly[22].temp - 273.15)} C°"
        //1
        timeHour23.text = "${(hour+23)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[23].weather[0].icon + ".png").into(iconHour23)
        hourTemp23.text = "${round(mWeather.hourly[23].temp - 273.15)} C°"
        //1
        timeHour24.text = "${(hour+24)%24}:00"
        Picasso.with(this).load("http://openweathermap.org/img/w/" +  mWeather.hourly[24].weather[0].icon + ".png").into(iconHour24)
        hourTemp24.text = "${round(mWeather.hourly[24].temp - 273.15)} C°"




    }
}



/* hourly excluded
*/

package com.example.weather

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class StartActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val array = doubleArrayOf(51.10,71.26)
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("array",array)
        startActivity(intent)
    }
}
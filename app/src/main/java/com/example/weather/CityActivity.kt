package com.example.weather

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_city.*
import kotlinx.android.synthetic.main.activity_city.view.*

class CityActivity: AppCompatActivity() {

    private val array = doubleArrayOf(0.0,0.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city)

        NurSultan.setOnClickListener(){
            send(51.10,71.26)
        }
        Almaty.setOnClickListener(){
            send(43.22,76.85)
        }
        NewYork.setOnClickListener(){
            send(40.71,-74.00)
        }
        Moscow.setOnClickListener(){
            send(55.75,37.61)
        }
        London.setOnClickListener(){
            send(51.30,0.08)
        }
        Tokyo.setOnClickListener(){
            send(35.41,139.42)
        }
        Paris.setOnClickListener(){
            send(48.51,48.51)
        }
        Cairo.setOnClickListener(){
            send(30.03,31.14)
        }
        Canberra.setOnClickListener(){
            send(-35.18,149.07)
        }
        Brazilia.setOnClickListener(){
            send(-15.48,-47.52)
        }
    }


    fun send(lat:Double, long:Double){
        array[0] = lat
        array[1] = long
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("array",array)
        startActivity(intent)
    }

}